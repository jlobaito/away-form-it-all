<?php
/**
 * @package contactsubmissions
 * @subpackage lexicon
 */
$_lang['prop_contactsubmissions.ascending']  = 'Ascending';
$_lang['prop_contactsubmissions.descending'] = 'Descending';
$_lang['prop_contactsubmissions.dir_desc']   = 'The direction to sort by.';
$_lang['prop_contactsubmissions.sort_desc']  = 'The field to sort by.';
$_lang['prop_contactsubmissions.tpl_desc']   = 'The chunk for displaying each row.';